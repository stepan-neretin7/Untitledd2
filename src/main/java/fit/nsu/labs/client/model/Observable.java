package fit.nsu.labs.client.model;

public interface Observable {
    void registerObserver(OnEvent o);
}